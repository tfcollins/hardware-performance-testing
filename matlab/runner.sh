#!/bin/bash

# Get latest master
git clone --recursive -b hw-tests https://github.com/analogdevicesinc/TransceiverToolbox.git
pwd
dir
cd TransceiverToolbox
# Run EVM tests
make -C CI/scripts add_libad9361
make -C CI/scripts test_evm
